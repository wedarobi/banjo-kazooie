#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_802C9E70(Actor *this);

/* .data */
extern ActorInfo D_80366C80 = { MARKER_53_EMPTY_HONEYCOMB, ACTOR_47_EMPTY_HONEYCOMB, ASSET_361_MODEL_EMPTY_HONEYCOMB, 
    0, NULL, 
    func_802C9E70, func_80326224, func_80325888, 
    { 0x0, 0x0}, 0, 0.8f, { 0x0, 0x0, 0x0, 0x0}
};

extern ActorInfo D_80366CA4 = { MARKER_55_HONEYCOMB, ACTOR_50_HONEYCOMB, ASSET_363_MODEL_HONEYCOMB, 
    0, NULL, 
    func_802C9E70, func_80326224, func_80325888, 
    { 0x0, 0x0}, 0, 0.8f, { 0x0, 0x0, 0x0, 0x0}
};

/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_42CB0/func_802C9C40.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_42CB0/func_802C9CF4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_42CB0/func_802C9D80.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_42CB0/func_802C9E70.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_42CB0/func_802CA1C4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_42CB0/func_802CA1CC.s")
